import { NbToastrModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../../environments/environment';
import { LocalesService } from '../servicios/locales.service';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    MiscellaneousModule,
    CommonModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'Chama'),
    NbToastrModule.forRoot()
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
   providers: [
    LocalesService
  ],
})
export class PagesModule {
}
