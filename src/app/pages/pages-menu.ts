import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    icon: 'nb-person',
    title: 'Clientes',
    link: '/pages/clientes/',
  },

];
