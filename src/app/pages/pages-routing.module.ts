
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      loadChildren: './../componentes/locales/locales.module#LocalesModule',
      path: 'locales'
    },
    {
      loadChildren: './../componentes/trabajadores/trabajadores.module#TrabajadoresModule',
      path: 'trabajadores'
    },
    {
      loadChildren: './../componentes/maquinas/maquinas.module#MaquinasModule',
      path: 'maquinas'
    },
    {
      loadChildren: './../componentes/clientes/clientes.module#ClientesModule',
      path: 'clientes'
    },
    {
      loadChildren: './../componentes/contadores/contadores.module#ContadoresModule',
      path: 'contadores'
    },
    {
      loadChildren: './../componentes/caja/caja.module#CajaModule',
      path: 'caja'
    },
    {
      loadChildren: './../componentes/sorteos/sorteos.module#SorteosModule',
      path: 'sorteos'
    },
    {
      loadChildren: './../componentes/finanzas/finanzas.module#FinanzasModule',
      path: 'finanzas'
    },
    {
      loadChildren: './../componentes/diaactual/diaactual.module#DiaactualModule',
      path: 'diaactual'
    },
    {
      loadChildren: './../componentes/perfil/perfil.module#PerfilModule',
      path: 'perfil'
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
