import { LocalesService } from './../../../servicios/locales.service';
import { Maquinas } from './../../../modelos/maquinas';

import { Component, OnInit, Pipe } from '@angular/core';
import { MaquinasService } from '../../../servicios/maquinas.service';
import { filter } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';





@Component({
  selector: 'agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})

export class AgregarComponent implements OnInit {
  selectedItem = '2';
  product = {} as Maquinas;
  private index: number = 0;
  constructor(private maquinasServ: MaquinasService,private locServ: LocalesService,private toastrService: NbToastrService) { }
  locales = [];
  ngOnInit() {
    this.locServ.getLocales().subscribe(loc => {
      this.locales = loc;
      console.log(this.locales);
      console.log("Maquinas cargadas!")
    });
  }
  addMaquina() {
    this.index += 1;
    this.toastrService.show(
      status || 'Maquina Creada con exito!',
      `Creando Maquina...`,
     );
      this.maquinasServ.addLocales(this.product);
      this.product = {} as Maquinas;


  }

  limpiar(){
    this.product = {} as Maquinas;
  }
}
