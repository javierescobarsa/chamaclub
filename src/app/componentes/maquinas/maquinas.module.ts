import { NgModule } from '@angular/core';

import { MaquinasRoutingModule } from './maquinas-routing.module';

@NgModule({
  imports: [

    MaquinasRoutingModule
  ]
})
export class MaquinasModule { }
