import { LocalesService } from './../../../servicios/locales.service';
import { Component, OnInit } from '@angular/core';
import { MaquinasService } from '../../../servicios/maquinas.service';


@Component({
  selector: 'listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss'],
})

export class ListarComponent implements OnInit {
   maquinas = [];
   locales = [];
  constructor(public maquinasServ: MaquinasService, public locServ: LocalesService) {
    this.maquinasServ.getLocales().subscribe(loc => {
      this.maquinas = loc;
      console.log(this.maquinas);
    });

    this.locServ.getLocales().subscribe(loc => {
      this.locales = loc;

    });
   }

  ngOnInit() {


  }

}
