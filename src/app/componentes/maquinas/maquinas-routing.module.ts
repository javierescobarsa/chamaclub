import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NbTabsetModule, NbInputModule, NbCardModule, NbSelectModule } from '@nebular/theme';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MaquinasComponent } from './maquinas.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';


const routes: Routes = [
  { path: '', component: MaquinasComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes),
    NbInputModule,
    NbTabsetModule,
    NbCardModule,
    CommonModule,
    NbSelectModule,
    FormsModule],
    declarations: [MaquinasComponent, ListarComponent,AgregarComponent ],
  exports: [RouterModule]
})
export class MaquinasRoutingModule {
  selectedItem = '2';

 }
