import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NbTabsetModule, NbInputModule, NbCardModule, NbSelectModule } from '@nebular/theme';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ContadoresComponent } from './contadores.component';
import { ListadoComponent } from './listado/listado.component';
import { AgregarComponent } from './agregar/agregar.component';
import { DiferenciasComponent } from './diferencias/diferencias.component';
import { BalanceComponent } from './balance/balance.component';


const routes: Routes = [
    { path: '', component: ContadoresComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes),
        NbInputModule,
        NbTabsetModule,
        NbCardModule,
        CommonModule,
        NbSelectModule,
        FormsModule],
    //Aqui se agregan los componentes que se desean cargar

    // -------> Componentes
    declarations: [ContadoresComponent,
        ListadoComponent,
        AgregarComponent,
        DiferenciasComponent,
        BalanceComponent
    ],
    // -------> Componentes
    exports: [RouterModule]
})
export class ContadoresRoutingModule {
    selectedItem = '2';

}
