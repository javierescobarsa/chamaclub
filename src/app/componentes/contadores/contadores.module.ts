import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContadoresRoutingModule } from './contadores-routing.module';

@NgModule({
  declarations: [],
  imports: [
    ContadoresRoutingModule
  ]
})
export class ContadoresModule { }
