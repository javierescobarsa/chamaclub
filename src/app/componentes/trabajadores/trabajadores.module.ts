import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrabajadoresRoutingModule } from './trabajadores-routing.module';


@NgModule({

  imports: [
    CommonModule,
    TrabajadoresRoutingModule
  ]
})
export class TrabajadoresModule { }
