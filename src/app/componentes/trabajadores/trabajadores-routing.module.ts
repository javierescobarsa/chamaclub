import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NbTabsetModule, NbInputModule, NbCardModule, NbSelectModule } from '@nebular/theme';
import { TrabajadoresComponent } from './trabajadores.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';


const routes: Routes = [
  { path: '', component: TrabajadoresComponent },

];
@NgModule({
  imports: [RouterModule.forChild(routes),NbTabsetModule,NbCardModule,NbInputModule],
  exports: [RouterModule],
  declarations: [TrabajadoresComponent, ListarComponent,AgregarComponent ],
})
export class TrabajadoresRoutingModule { }
