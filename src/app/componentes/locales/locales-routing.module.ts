import { NbTabsetModule, NbInputModule, NbCardModule } from '@nebular/theme';
import { AgregarComponent } from './agregar/agregar.component';
import { LocalesComponent } from './locales.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoComponent } from './listado/listado.component';
import { LocalesService } from '../../servicios/locales.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
const routes: Routes = [
  { path: '', component: LocalesComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes),NbTabsetModule,NbInputModule,CommonModule,FormsModule,  NbCardModule],
  exports: [RouterModule],
  declarations: [LocalesComponent, ListadoComponent,AgregarComponent ],
     providers: [LocalesService]
})
export class LocalesRoutingModule { }
