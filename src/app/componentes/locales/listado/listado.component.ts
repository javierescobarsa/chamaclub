
import { Component, OnInit } from '@angular/core';


import { LocalesService } from '../../../servicios/locales.service';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})


export class ListadoComponent implements OnInit {
  loc=[];
  constructor(public localesServ: LocalesService) {
    this.localesServ.getLocales().subscribe(loc => {
      this.loc = loc;
      console.log(this.loc);
    });
   }

  ngOnInit() {




  }
  ngOnDestroy() {

  }


}
