
import { Locales } from './../../../modelos/locales';
import { Component, OnInit } from '@angular/core';
import { LocalesService } from '../../../servicios/locales.service';



@Component({
  selector: 'agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {
  product = {} as Locales;
  constructor(private productService: LocalesService) { }

  ngOnInit() {
  }
  addLocal() {
    if(this.product.nombre !== '' && this.product.fono!== '' && this.product.ciudad != '') {
      this.productService.addLocales(this.product);
      this.product = {} as Locales;
    }
  }
  limpiar(){
    this.product = {} as Locales;
  }
}
