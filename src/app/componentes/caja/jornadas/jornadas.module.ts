import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JornadasRoutingModule } from './jornadas-routing.module';
import { JornadasComponent } from './jornadas.component';

@NgModule({
  declarations: [JornadasComponent],
  imports: [
    CommonModule,
    JornadasRoutingModule
  ]
})
export class JornadasModule { }
