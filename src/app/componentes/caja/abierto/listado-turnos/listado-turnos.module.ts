import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListadoTurnosComponent } from './listado-turnos.component';

@NgModule({
  declarations: [ListadoTurnosComponent],
  imports: [
    CommonModule
  ]
})
export class ListadoTurnosModule { }
