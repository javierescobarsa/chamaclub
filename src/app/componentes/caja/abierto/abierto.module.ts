import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbiertoRoutingModule } from './abierto-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AbiertoRoutingModule
  ]
})
export class AbiertoModule { }
