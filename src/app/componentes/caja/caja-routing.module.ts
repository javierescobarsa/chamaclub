import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CerradoComponent } from './cerrado/cerrado.component';
import { TurnosComponent } from './turnos/turnos.component';
import { NbCardModule, NbListComponent, NbUserModule, NbListModule } from '@nebular/theme';
import { CommonModule } from '@angular/common';


const routes: Routes = [
  {
    component: CerradoComponent ,
    path: ''
  },
  {
    component: CerradoComponent ,
    path: 'cerrado'
  },
  {
    component: TurnosComponent ,
    path: 'turnos'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes),NbCardModule,NbListModule,CommonModule,NbUserModule ],
  declarations:[CerradoComponent,TurnosComponent ],
  exports: [RouterModule]
})
export class CajaRoutingModule { }
