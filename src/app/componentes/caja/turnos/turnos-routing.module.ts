import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TurnosComponent } from './turnos.component';

const routes: Routes = [
  {
    component: TurnosComponent ,
    path: ''
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations:[TurnosComponent],
  exports: [RouterModule]
})
export class TurnosRoutingModule { }
