import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CajaRoutingModule } from './caja-routing.module';
import { CajaComponent } from './caja.component';
import { AbiertoComponent } from './abierto/abierto.component';
import { NbCardModule, NbUserModule, NbListModule } from '@nebular/theme';

@NgModule({
  declarations: [CajaComponent, AbiertoComponent],
  imports: [NbCardModule,
    CommonModule,
    CajaRoutingModule,
    NbUserModule,
    NbListModule
  ]
})
export class CajaModule { }
