import { LocalesService } from './../../../servicios/locales.service';
import { Clientes } from './../../../modelos/clientes';

import { Component, OnInit, Pipe } from '@angular/core';
import { ClientesService } from '../../../servicios/clientes.service';
import { filter } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';





@Component({
  selector: 'agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})

export class AgregarComponent implements OnInit {
  selectedItem = '2';
  cliente = {} as Clientes;
  private index: number = 0;
  constructor(private clientesServ: ClientesService, private toastrService: NbToastrService) { }
  clientes = [];
  ngOnInit() {
    this.clientesServ.getClientes().subscribe(loc => {
      this.clientes = loc;

    });
  }
  addCliente() {
    // Existe el rut anteriormente?
    // El rut es correcto?
    //
    this.index += 1;
    this.toastrService.show(
      status || 'Cliente Creado con exito!',
      `Creando Cliente...`,
    );
    this.cliente.rut = this.cliente.rut + '-' + this.cliente.dv;
    if (this.clientesServ.validarRut(this.cliente) === 1) {
console.log('dv valido')
    }else{
      console.log('dv no valido')
    }
    this.clientesServ.addClientes(this.cliente);
    this.cliente = {} as Clientes;


  }

  limpiar() {
    this.cliente = {} as Clientes;
  }
}
