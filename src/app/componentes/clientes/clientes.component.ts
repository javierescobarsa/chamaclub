import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../../servicios/clientes.service';

@Component({
  selector: 'clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
   clientes = [];
  constructor(public clientesServ: ClientesService) {  
    this.clientesServ.getClientes().subscribe(cli => {
      this.clientes = cli;
      console.log(this.clientes);
    }); }

  ngOnInit() {
  }

}
