import { LocalesService } from './../../../servicios/locales.service';
import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../../../servicios/clientes.service';


@Component({
  selector: 'listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss'],
})

export class ListarComponent implements OnInit {
   clientes = [];
  constructor(public clientesServ: ClientesService) {
    this.clientesServ.getClientes().subscribe(cli => {
      this.clientes = cli;
      console.log(this.clientes);
    });


   }

  ngOnInit() {


  }

}
