import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SisCajaRoutingModule } from './sis-caja-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SisCajaRoutingModule
  ]
})
export class SisCajaModule { }
