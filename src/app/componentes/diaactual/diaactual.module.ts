import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiaactualRoutingModule } from './diaactual-routing.module';
import { DiaactualComponent } from './diaactual.component';
import { NbCardModule } from '@nebular/theme';

@NgModule({
  
  imports: [
    CommonModule,
    DiaactualRoutingModule,
    NbCardModule
  ],
  declarations: [DiaactualComponent]
})
export class DiaactualModule { }
