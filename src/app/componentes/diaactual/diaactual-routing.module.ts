import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiaactualComponent } from './diaactual.component';
import { ListarComponent } from './listar/listar.component';
import { NbCardModule } from '@nebular/theme';

const routes: Routes = [
  {
    component: ListarComponent  ,
    path: ''
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes),
  NbCardModule],
  exports: [RouterModule],
  declarations:[ListarComponent]

})
export class DiaactualRoutingModule { }
