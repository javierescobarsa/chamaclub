import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajabaseComponent } from './cajabase.component';

describe('CajabaseComponent', () => {
  let component: CajabaseComponent;
  let fixture: ComponentFixture<CajabaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajabaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
