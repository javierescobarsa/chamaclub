import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiaactualComponent } from './diaactual.component';

describe('DiaactualComponent', () => {
  let component: DiaactualComponent;
  let fixture: ComponentFixture<DiaactualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaactualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiaactualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
