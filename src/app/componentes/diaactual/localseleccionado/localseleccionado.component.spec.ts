import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalseleccionadoComponent } from './localseleccionado.component';

describe('LocalseleccionadoComponent', () => {
  let component: LocalseleccionadoComponent;
  let fixture: ComponentFixture<LocalseleccionadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalseleccionadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalseleccionadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
