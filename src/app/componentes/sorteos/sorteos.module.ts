import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SorteosRoutingModule } from './sorteos-routing.module';

@NgModule({
  
  imports: [
    CommonModule,
    SorteosRoutingModule
  ],
  

})
export class SorteosModule { }
