import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { NbTabsetModule, NbInputModule, NbCardModule, NbSelectModule } from '@nebular/theme';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SorteosComponent } from './sorteos.component';
import { AgregarComponent } from './agregar/agregar.component';
import { HistorialComponent } from './historial/historial.component';

const routes: Routes = [
  {
    component: SorteosComponent ,
    path: ''
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes),
    NbInputModule,
    NbTabsetModule,
    NbCardModule,
    CommonModule,
    NbSelectModule,
    FormsModule],

     // -------> Componentes
     declarations: [SorteosComponent,
      ListarComponent,
      AgregarComponent,
      HistorialComponent,
     
  ],
  // -------> Componentes
  
  exports: [RouterModule]
})
export class SorteosRoutingModule { }
