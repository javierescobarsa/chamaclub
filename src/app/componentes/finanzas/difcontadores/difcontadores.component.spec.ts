import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DifcontadoresComponent } from './difcontadores.component';

describe('DifcontadoresComponent', () => {
  let component: DifcontadoresComponent;
  let fixture: ComponentFixture<DifcontadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DifcontadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DifcontadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
