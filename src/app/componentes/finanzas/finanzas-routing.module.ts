import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinanzasComponent } from './finanzas.component';
import { SelectorfechaComponent } from './selectorfecha/selectorfecha.component';
import { NbCardModule } from '@nebular/theme';

const routes: Routes = [
  {
    component: SelectorfechaComponent ,
    path: ''
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes),
    NbCardModule],
  exports: [RouterModule],
  declarations: [FinanzasComponent,SelectorfechaComponent],
})
export class FinanzasRoutingModule { }
