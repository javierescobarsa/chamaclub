import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiaseleccionadoComponent } from './diaseleccionado.component';

describe('DiaseleccionadoComponent', () => {
  let component: DiaseleccionadoComponent;
  let fixture: ComponentFixture<DiaseleccionadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiaseleccionadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiaseleccionadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
