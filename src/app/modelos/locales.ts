export interface Locales {
    id?: string;
    nombre: string;
    direccion: string;
    fono: string;
    ciudad:string;
  }
