export interface Clientes {
    id?: string;
    nombre: string;
    apellido: string;
    rut: string;
    dv?: string;
    fecha?: Date;
    clasificacion?: string;


  }
