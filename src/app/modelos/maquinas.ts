export interface Maquinas {
  id?: string;
  numero: number;
  in: number;
  out: number;
  multiplicador: number;
  balance: number;
  fecha:string;
  local:string;
  idLocal:string;

}
