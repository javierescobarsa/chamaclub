import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Administracion de clientes <b></b> 2019</span>
    <div class="socials">

    </div>
  `,
})
export class FooterComponent {
}
