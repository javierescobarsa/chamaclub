import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Clientes } from '../modelos/clientes';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class ClientesService {
  ClientesList: AngularFirestoreCollection<Clientes>;
  Clientes: Observable<Clientes[]>;
  ClientesDoc: AngularFirestoreDocument<Clientes>;
  controlRut = false;
  constructor(public db: AngularFirestore) {
    this.ClientesList = this.db.collection('clientes');
    this.Clientes = this.ClientesList.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Clientes;
        data.id = a.payload.doc.id;

        return data;
      });
    }));
  }

  getClientes() {
    return this.Clientes;
  }
  validarRut(cliente: Clientes) {
    this.controlRut = false;
    // preguntamos si el digito verificador es igual a u nnumero del 1 al 9
    for (let index = 0; index < 10; index++) {
      if (cliente.dv === '' + index) {
        this.controlRut = true;
      }
    }
    if (cliente.dv === 'k') {
      this.controlRut = true;
    }

    if(this.controlRut==true){
      return 1;
    }else{
      return 2;
    }
  }
  addClientes(cliente: Clientes) {
    this.ClientesList.add(cliente);
  }

  deleteClientes(cliente: Clientes) {
    this.ClientesDoc = this.db.doc(`clientes/${cliente.id}`);
    this.ClientesDoc.delete();
  }

  updateClientes(cliente: Clientes) {
    this.ClientesDoc = this.db.doc(`clientes/${cliente.id}`);
    this.ClientesDoc.update(cliente);
  }

}
