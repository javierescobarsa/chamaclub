import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Maquinas } from '../modelos/maquinas';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class MaquinasService {
  maquinasList: AngularFirestoreCollection<Maquinas>;
  maquinas: Observable<Maquinas[]>;
  maquinasDoc: AngularFirestoreDocument<Maquinas>;
  constructor(public db: AngularFirestore) {
    this.maquinasList = this.db.collection('maquinas');
    this.maquinas = this.maquinasList.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Maquinas;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
   }

   getLocales() {
    return this.maquinas;
  }

  addLocales(local: Maquinas) {
    this.maquinasList.add(local);
  }

  deleteLocales(local: Maquinas) {
    this.maquinasDoc = this.db.doc(`maquinas/${local.id}`);
    this.maquinasDoc.delete();
  }

  updateLocales(local: Maquinas) {
    this.maquinasDoc = this.db.doc(`maquinas/${local.id}`);
    this.maquinasDoc.update(local);
  }

}
