import { TestBed } from '@angular/core/testing';

import { SorteosService } from './sorteos.service';

describe('SorteosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SorteosService = TestBed.get(SorteosService);
    expect(service).toBeTruthy();
  });
});
