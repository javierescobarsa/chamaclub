import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Locales } from '../../modelos/locales';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class LocalesService {
  localesList: AngularFirestoreCollection<Locales>;
  locales: Observable<Locales[]>;
  localesDoc: AngularFirestoreDocument<Locales>;
  constructor(public db: AngularFirestore) {
    this.localesList = this.db.collection('locales');
    this.locales = this.localesList.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Locales;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
   }

   getLocales() {
    return this.locales;
  }

  addLocales(local: Locales) {
    this.localesList.add(local);
  }

  deleteLocales(local: Locales) {
    this.localesDoc = this.db.doc(`locales/${local.id}`);
    this.localesDoc.delete();
  }

  updateLocales(local: Locales) {
    this.localesDoc = this.db.doc(`locales/${local.id}`);
    this.localesDoc.update(local);
  }

}
